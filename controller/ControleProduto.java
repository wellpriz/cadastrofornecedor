
package controller;

/**
 *
 * @author well
 * 
 */

import java.util.ArrayList;
import model.Produto;
public class ControleProduto {

   private ArrayList<Produto> listadeProduto;
    
    public ControleProduto() {
        
        listadeProduto = new ArrayList<Produto>();
    }
    
    
     public String adicionar (Produto umProduto) {
        listadeProduto.add(umProduto);
        return "Produto adicionado com sucesso.";
    }
    
     public String remover (Produto umProduto) {
        listadeProduto.remove(umProduto);
        return "Produto removido com sucesso.";
    } 
     
    public String TipoFornecedor( Produto umProduto){
    
        if( umProduto.getUmFornecedor().getIdentificador().
                equalsIgnoreCase("pessoa Juridica)"))
            return "pessoa Juridica";
        else
            return "pessoa fisica";
        
    }
    public Produto pesquisarFornecedor(String umProduto) {
        for (Produto b: listadeProduto) {
            if (b.getNome().equalsIgnoreCase(umProduto)) 
                return b;
        }
        return null;
    }
   
 
    
    
    
}
