package controller;

import java.util.ArrayList;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;

public class ControleFornecedor {

    private ArrayList<Fornecedor> listaFornecedores;
    
    public ControleFornecedor(){
        listaFornecedores = new ArrayList<Fornecedor>();
    }
        
    public String adicionar (PessoaFisica fornecedor) {
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Física adicionado com sucesso.";
    }
    
    public String adicionar (PessoaJuridica fornecedor) {
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Jurídica adicionado com sucesso.";
    }
    
    public String PesquisarTipo(Fornecedor umfornecedor){
    
        for (Fornecedor b: listaFornecedores){
                if (b.getIdentificador().equalsIgnoreCase("pessoa fisica")){
                    return b.getIdentificador();
      
                }
                if(b.getIdentificador().equalsIgnoreCase("pessoa Juridica")){
                        
                    return b.getIdentificador();
                }     
        }
        return "Nenhum Fornecedor encontrado";
    }
    
 public Fornecedor pesquisarFornecedor(String nome) {
        for (Fornecedor b: listaFornecedores) {
            if (b.getNome().equalsIgnoreCase(nome)) return b;
        }
        return null;
    }
    
    public String remover (PessoaFisica fornecedor) {
        listaFornecedores.remove(fornecedor);
        return "Fornecedor Pessoa Física adicionado com sucesso.";
    }
    
    public String remover (PessoaJuridica fornecedor) {
        listaFornecedores.remove(fornecedor);
        return "Fornecedor Pessoa Jurídica adicionado com sucesso.";
    }
    
}
