
package model;

public class PessoaJuridica extends Fornecedor{
    private String cnpj;
    private String razaoSocial;

   
    
    public PessoaJuridica(String umNome, String umTelefone, String umcnpj, String umaRazaaoSocial){
    
    super(umNome, umTelefone);
    
    this.cnpj =  umcnpj;
    this.razaoSocial = umaRazaaoSocial; 
    
    }
    
    
    
    
    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    
    
}
