
package model;

public class PessoaFisica extends Fornecedor{
    private String cpf;

  
    
    public PessoaFisica(String umNome, String umTelefone, String umcpf){
    
    super(umNome, umTelefone);
    
    this.cpf =  umcpf;
    
    this.identificador = "Pessoa Física";
    
    }
    
      public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
}
